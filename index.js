/*

 Так как мы не старые тупорылые обмудки из 80-х, будем использовать
 XMLHttpRequest, а не ActiveX. Пускай это работает где-нибудь кроме
 IE6 :)

 */


// Собственно, создаём объект запроса
var xmlRequest = new XMLHttpRequest();

// Когда index.html сформируется полностью, вызываем
// функцию, которая отобразит старое значение элемента,
// которое будет изменятся. По сути, это всегда значение
// первого элемента списка.

xmlRequest.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        showOldValue(this);
    }
};

// Формиурем (open) и посылаем  (send) AJAX-запрос
// (ого-го!) и получаем наш XML-файл

// Возможно, кого-то интересует, что здесь
// означает "true"? Оно посылает одного ВИЧ-позитивного
// в Светлогорск.

xmlRequest.open("GET", "index.xml", true);
xmlRequest.send();

function showOldValue(xml) {
    /*
     * Эта функция просто для того, чтобы
     * отображать старое значение по загрузке страницы
     * либо при перевыборе элемента из списка
     */

    // Получаем ответ в виде XML-структуры
    var xmlDoc = xml.responseXML;

    // Получаем выбранный элемент списка
    var osid = document.getElementById("osId").selectedIndex;

    // Получаем старое значение элемента "name" (ну или какой там у вас будет)
    var oldValue = xmlDoc.getElementsByTagName("name")[osid].childNodes[0].nodeValue;

    // Выводим старое значение
    document.getElementById("oldValue").innerHTML = oldValue;
}

function changeData(xml) {

    // Получаем ответ в виде XML-структуры
    var xmlDoc = xml.responseXML;

    // Получаем номер того блока, поля которого хотим менять:
    var osid = document.getElementById("osId").selectedIndex;

    // Получаем значение, которым "заменим" старое
    var newvalue = document.getElementById("newValue").value;

    // Получаем элемент в DOM-дереве, который будем менять
    var selected_node = xmlDoc.getElementsByTagName("name")[osid].childNodes[0];

    // Обновляем значение
    selected_node.nodeValue = newvalue;

    // Отображаем изменения
    showSrc(xmlDoc);
}

function addNode(xml) {

    // Получаем ответ в виде XML-структуры
    var xmlDoc = xml.responseXML;

    // Получаем номер того блока, поля которого хотим менять:
    var osid = document.getElementById("osId").selectedIndex;

    // Я добавлял элемент "ссылка", поэтому у меня
    // будут поля для адреса и текста ссылки
    var hrefto = document.getElementById("hrefTo");
    var hrefval = document.getElementById("hrefVal");

    // Создаём элемент <link>, в который
    // вложим элемент <a>
    var link_node = document.createElement("link");
    var a_node = document.createElement("a");

    // Добавляем атрибут "href" элементу "a"
    a_node.setAttribute("href", hrefto.value);

    // Создаём элемент-текст ссылки
    var textnode = document.createTextNode(hrefval.value);

    // Вкладываем элемент "a" в "link"
    link_node.appendChild(a_node);

    // Вкладываем элемент "текст ссылки" в "a"
    a_node.appendChild(textnode);

    // Вкладываем элемент "link" в наше дерево
    xmlDoc.getElementsByTagName("notesLinks")[osid].appendChild(link_node);

    // Очищаем поля
    hrefto.value = '';
    hrefval.value = '';

    // Отображаем изменения
    showSrc(xmlDoc);
}

function deleteNode(xml) {

    // Получаем ответ в виде XML-структуры
    var xmlDoc = xml.responseXML;

    // Получаем номер того блока, поля которого хотим менять:
    var osid = document.getElementById("osId").selectedIndex;

    // Получаем дочерние элементы, выбираем последний
    var notesLinks = xmlDoc.getElementsByTagName("notesLinks")[osid];
    var last_link = notesLinks.lastElementChild;

    // Удаляем
    // Выпендримся обработчиком ошибок.
    try {
        xmlDoc.getElementsByTagName("notesLinks")[osid].removeChild(last_link);
    } catch (TypeError) {
        alert("В данном блоке все элементы link закончились!");
    }

    // Отображаем изменения
    showSrc(xmlDoc);
}

function replaceNode(xml) {
    // Получаем ответ в виде XML-структуры
    var xmlDoc = xml.responseXML;

    // Получаем номер того блока, поля которого хотим менять:
    var osid = document.getElementById("osId").selectedIndex;

    // Получаем дочерние элементы, выбираем последний
    var notesLinks = xmlDoc.getElementsByTagName("notesLinks")[osid];
    var last_link = notesLinks.lastElementChild;

    // Элемент, который заменит предыдущий
    var img_node = document.createElement("img");

    // Добавляем атрибут "src" элементу "img"
    img_node.setAttribute("src", "pattern.png");

    // Производим замену
    var res = notesLinks.replaceChild(img_node, notesLinks.firstElementChild);

    // Отображаем изменения
    showSrc(xmlDoc);
}

function showSrc(x) {

    // Вот эта штука нужна, чтобы вывести чистый изменённый XML/DOM-код как текст.
    // То, как описано в методичке, есесна работает только в IE.
    // x - xmlDoc

    var Xml2String = (x.xml) ? x.xml : new XMLSerializer().serializeToString(x);

    // Далее показываем этот текст пользователю (или ружицкой хыхыхыхыхых)
    document.getElementById("demo").innerHTML = Xml2String;

}

function hideSrc() {

    // Это для кнопки "спрятать". По сути, просто убираем весь текст,
    // который выводили кнопкой "Изменить"
    document.getElementById("demo").innerHTML = 'Пока здесь ничего...';
}
